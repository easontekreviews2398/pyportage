#!/usr/bin/env python3

import parser
import sys
import os
import shutil
from styles import *

# code
def exists(cmd):
    return shutil.which(cmd) is not None

def rootify():
    rootcmds = ['sudo', 'doas']
    for cmd in rootcmds:
        if exists(cmd):
            c = [sys.executable] + sys.argv + [os.environ]
            os.execlpe(cmd, cmd, *c)
            # root privs gained
    return

def prechecks() -> bool:
    # OS CHECK
    if sys.platform != "linux":
        print(format_as("fatalerror", "this portage frontend does not work on non-linux systems!"))
        return False
    
    # DEPENDENCY CHECK
    try:
        import distro
    except ImportError:
        print(format_as("fatalerror", "the distro package is not installed! please install the dependencies properly before continuing."))
        return False

    # DISTRO CHECK
    if distro.name() != "Gentoo":
        print(format_as("fatalerror", "This portage frontend only works on gentoo for now, add support for other gentoo-like distros yourself."))
        return False
    
    # Rootify if not root
    if os.geteuid() != 0:
        print(format_as("notice", "Program not started as root, gaining root privs."))
        rootify()
    
    return True

def usage() -> str:
    return f"""
    {Formatting.BOLD}USAGE:{Formatting.ENDBOLD}

    {format_as("info", "not implemented")}
    """


def main(args: list):
    if os.geteuid() != 0:
        if not prechecks():
            print(format_as("info", "Certain requirements were not met. Please read the errors and attempt to fix them.\n exiting..."))
            return
    
    # main logic starts after root privs are gained
    if len(args) == 0:
        print(format_as("notice", "No arguments specified."))
        print(usage())


if __name__ == "__main__": main(sys.argv[1:]); exit()
