# PyPortage

A stupid-simple portage frontend written in Python to keep things in one place with prettier warnings and better search. Similar to using `yay` or `yaourt` instead of just `pacman` on arch
