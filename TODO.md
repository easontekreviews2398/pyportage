* finnish sanity checks for the emerge backend
* make classes to keep things modular
* actually getting the backend somewhat working
* prettyify warnings
* allow for `emerge --pretend` and non root commands
* **SEARCH ENGINE**
* publish on PyPI
* put on reddit (r/Python or r/Gentoo or both)
* Prettyify output w/ stdout scanning
