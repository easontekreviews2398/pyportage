class Formatting:
    LIGHTRED= '\x1b[91m'
    BLACK   = '\x1b[30m'
    BLUE    = '\x1b[34m'
    CYAN    = '\x1b[36m'
    GREEN   = '\x1b[32m'
    MAGENTA = '\x1b[35m'
    RED     = '\x1b[31m'
    YELLOW  = '\x1b[33m'
    WHITE   = '\x1b[37m'

    BOLD    = '\033[1m'
    ENDBOLD = '\033[0m'

    RESET   = '\x1b[39m'

# formatting
def format_as(T: str, inp: str) -> str:
    if T == "error":
        return Formatting.BOLD + Formatting.LIGHTRED + "==> " + Formatting.RESET + Formatting.ENDBOLD + inp
    if T == "fatalerror":
        return Formatting.BOLD + Formatting.RED + "==> " + Formatting.RESET + inp + Formatting.ENDBOLD
    if T == "notice":
        return Formatting.BOLD + Formatting.YELLOW + "==> " + Formatting.RESET + Formatting.ENDBOLD + inp
    if T == "info":
        return Formatting.BOLD + Formatting.GREEN + "==> " + Formatting.RESET + Formatting.ENDBOLD + inp
    return inp
