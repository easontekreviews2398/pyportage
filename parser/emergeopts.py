from dataclasses import dataclass
from . import EmergeFlag

@dataclass
class EmergeOpts():
    """
    95% of all emerge options.
    """
    
    alert                   : EmergeFlag          = EmergeFlag(flag_name="alert",
                                                               enabled=True,
                                                               opts=None) # on for safety and polish

    alphabetical            : EmergeFlag          = EmergeFlag(flag_name="alphabetical",
                                                               enabled=False,
                                                               opts=None)
    ask                     : EmergeFlag          = EmergeFlag(flag_name="ask",
                                                               enabled=True,
                                                               opts=None)   # on for safety
    ask_enter_invalid       : EmergeFlag          = EmergeFlag(flag_name="ask_enter_invalid",
                                                               enabled=False,
                                                               opts=None)   # on for safety (again)
    autounmask              : EmergeFlag          = EmergeFlag(flag_name="autounmask",
                                                               enabled=False,
                                                               opts=None)
    autounmask_only         : EmergeFlag          = EmergeFlag(flag_name="autounmask_only",
                                                               enabled=False,
                                                               opts=None)
    autounmask_continue     : EmergeFlag          = EmergeFlag(flag_name="autounmask_continue",
                                                               enabled=False,
                                                               opts=None)
    autounmask_license      : EmergeFlag          = EmergeFlag(flag_name="autounmask_license",
                                                               enabled=False,
                                                               opts=None)
    autounmask_use          : EmergeFlag          = EmergeFlag(flag_name="autounmask_use",
                                                               enabled=False,
                                                               opts=None)
    autounmask_write        : EmergeFlag          = EmergeFlag(flag_name="autounmask_write",
                                                               enabled=False,
                                                               opts=None)
    backtrack               : EmergeFlag          = EmergeFlag(flag_name="backtrack",
                                                               enabled=True,
                                                               opts=10)     # default, as specified in the manpage
    binpkg_changed_deps     : EmergeFlag          = EmergeFlag(flag_name="binpkg_changed_deps",
                                                               enabled=True, # default
                                                               opts=None)
    buildpkg                : EmergeFlag          = EmergeFlag(flag_name="buildpkg",
                                                               enabled=False,
                                                               opts=None)
    buildpkg_exclude        : EmergeFlag          = EmergeFlag(flag_name="buildpkg_exclude",
                                                               enabled=False,
                                                               opts=None)
    buildpkg_only           : EmergeFlag          = EmergeFlag(flag_name="buildpkg_only",
                                                               enabled=False,
                                                               opts=None)
    changed_deps            : EmergeFlag          = EmergeFlag(flag_name="changed_deps",
                                                               enabled=False,
                                                               opts=None)
    changed_deps_report     : EmergeFlag          = EmergeFlag(flag_name="changed_deps_report",
                                                               enabled=False,
                                                               opts=None)
    changed_use             : EmergeFlag          = EmergeFlag(flag_name="changed_use",
                                                               enabled=False,
                                                               opts=None)
    color                   : EmergeFlag          = EmergeFlag(flag_name="color",
                                                               enabled=True,
                                                               opts=None)  # on as specified in the manpage
    complete_graph          : EmergeFlag          = EmergeFlag(flag_name="complete_graph",
                                                               enabled=False,
                                                               opts=None)
    deep                    : EmergeFlag          = EmergeFlag(flag_name="deep",
                                                               enabled=False,
                                                               opts=10) # default is 10
    digest                  : EmergeFlag          = EmergeFlag(flag_name="digest",
                                                               enabled=False,
                                                               opts=None)
    dynamic_deps            : EmergeFlag          = EmergeFlag(flag_name="dynamic_deps",
                                                               enabled=False,
                                                               opts=None)
    exclude                 : EmergeFlag          = EmergeFlag(flag_name="exclude",
                                                               enabled=False,
                                                               opts=[])
    empty_tree              : EmergeFlag          = EmergeFlag(flag_name="empty_tree",
                                                               enabled=False,
                                                               opts=None)
    fail_clean              : EmergeFlag          = EmergeFlag(flag_name="fail_clean",
                                                               enabled=False,
                                                               opts=None)
    fetch_only              : EmergeFlag          = EmergeFlag(flag_name="fetch_only",
                                                               enabled=False,
                                                               opts=None)
    fuzzy_search            : EmergeFlag          = EmergeFlag(flag_name="fuzzy_search",
                                                               enabled=False,
                                                               opts=None)
    getbinpkg               : EmergeFlag          = EmergeFlag(flag_name="getbinpkg",
                                                               enabled=False,
                                                               opts=None) # noone uses binpkgs (mostly)
    getbinpkg_only          : EmergeFlag          = EmergeFlag(flag_name="getbinpkg_only",
                                                               enabled=False,
                                                               opts=None)
    ignore_world            : EmergeFlag          = EmergeFlag(flag_name="ignore_world",
                                                               enabled=False,
                                                               opts=None) # duh its stupid to have it on
    newrepo                 : EmergeFlag          = EmergeFlag(flag_name="newrepo",
                                                               enabled=False,
                                                               opts=None)
    newuse                  : EmergeFlag          = EmergeFlag(flag_name="newuse",
                                                               enabled=False,
                                                               opts=None)
    nodeps                  : EmergeFlag          = EmergeFlag(flag_name="nodeps",
                                                               enabled=False,
                                                               opts=None)
    noreplace               : EmergeFlag          = EmergeFlag(flag_name="noreplace",
                                                               enabled=False,
                                                               opts=None)
    oneshot                 : EmergeFlag          = EmergeFlag(flag_name="oneshot",
                                                               enabled=False,
                                                               opts=None)
    onlydeps_with_rdeps     : EmergeFlag          = EmergeFlag(flag_name="onlydeps_with_rdeps",
                                                               enabled=True,
                                                               opts=None)  # as specified by the manpage
    pkg_format              : EmergeFlag          = EmergeFlag(flag_name="pkg_format",
                                                               enabled=False,
                                                               opts="tar") # can be tar or rpm
    prefix                  : EmergeFlag          = EmergeFlag(flag_name="prefix",
                                                               enabled=False,
                                                               opts=None)
    pretend                 : EmergeFlag          = EmergeFlag(flag_name="pretend",
                                                               enabled=False,
                                                               opts=None) # useful tho
    quickpkg_direct         : EmergeFlag          = EmergeFlag(flag_name="quickpkg_direct",
                                                               enabled=False,
                                                               opts=None)
    quickpkg_direct_dir     : EmergeFlag          = EmergeFlag(flag_name="quickpkg_direct_dir",
                                                               enabled=False,
                                                               opts=None) # value depends on above option
    quiet                   : EmergeFlag          = EmergeFlag(flag_name="quiet",
                                                               enabled=False,
                                                               opts=None) # portage tries to make itself shut up
    quiet_build             : EmergeFlag          = EmergeFlag(flag_name="quiet_build",
                                                               enabled=False,
                                                               opts=None) # redirects build logs to log files away from stdout
    quiet_fail              : EmergeFlag          = EmergeFlag(flag_name="quiet_fail",
                                                               enabled=False,
                                                               opts=None) # shut up even if it fails
    rage_clean              : EmergeFlag          = EmergeFlag(flag_name="rage_clean",
                                                               enabled=False,
                                                               opts=None) # NEVER TURN THIS ON
    search_index            : EmergeFlag          = EmergeFlag(flag_name="search_index",
                                                               enabled=True,
                                                               opts=None)  # as specified by the manpage
    search_similarity       : EmergeFlag          = EmergeFlag(flag_name="search_similarity",
                                                               enabled=False,
                                                               opts=80.0)
    select                  : EmergeFlag          = EmergeFlag(flag_name="select",
                                                               enabled=False,
                                                               opts=None)
    tree                    : EmergeFlag          = EmergeFlag(flag_name="tree",
                                                               enabled=False,
                                                               opts=None)
    update                  : EmergeFlag          = EmergeFlag(flag_name="update",
                                                               enabled=False,
                                                               opts=None)
    verbose                 : EmergeFlag          = EmergeFlag(flag_name="verbose",
                                                               enabled=False,
                                                               opts=None)
    verbose_conflicts       : EmergeFlag          = EmergeFlag(flag_name="verbose_conflicts",
                                                               enabled=False,
                                                               opts=None)
    verbose_slot_rebuilds   : EmergeFlag          = EmergeFlag(flag_name="verbose_slot_rebuilds",
                                                               enabled=False,
                                                               opts=None)
    with_bdeps              : EmergeFlag          = EmergeFlag(flag_name="with_bdeps",
                                                               enabled=True,
                                                               opts=None)  # as specified by the manpage
    with_bdeps_auto         : EmergeFlag          = EmergeFlag(flag_name="with_bdeps_auto",
                                                               enabled=True,
                                                               opts=None)  # as specified by the manpage 
