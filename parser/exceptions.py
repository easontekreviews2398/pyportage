from ..styles import *

class InvalidEmergeFlagNameError(Exception):
    def __init__(self, invalid_flag_name :str) -> None:
        super().__init__(self, f'{format_as("fatalerror", f"Invalid Flag name {invalid_flag_name}! exiting...")}')
