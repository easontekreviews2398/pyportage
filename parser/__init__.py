from .exceptions import InvalidEmergeFlagNameError
from .emergeopts import EmergeOpts 

"""
PyPortage command parser.

subcommands that should be implemented:
    * emerge
        * sync
        * emerging @system, @world, atoms and tbz2's 
        FEATURES SUCH AS:
        * --ask
        * --autounmask=(y/n)
        * --jobs
        * --oneshot
        * --columns
        * --verbose
        * --newuse
        * --with-bdeps=(y/n)
        * --nospinner
    * emaint (not needed...?)
    * layman (if present, may require as dependency)
    * dispatch-conf (beacuse yes)
    * emerge-webrsync

"""

class EmergeFlag():
    def __init__(self, 
                 flag_name: str,
                 enabled: bool,
                 opts: (float | int | str | list[str] | None),
                 desc: str = "") -> None:

        self.flag_name: str                                 = flag_name
        self.enabled: bool                                  = enabled
        self.opts: (float | int | str | list[str] | None)   = opts
        self.desc: str                                      = desc

        self.fmt_table = {
            "alert" : lambda: "--alert=y" if self.enabled else "",
            "alphabetical": lambda: "--alphabetical" if self.enabled else "",
            "ask" : lambda: "--ask=y" if self.enabled else "",
            "ask_enter_invalid" : lambda: "--ask-enter-invalid" if self.enabled else "",
        }

    def _format_flag_as_str(self) -> str:
        return ""

    def __repr__(self) -> str:
        try:
            return self.fmt_table[self.flag_name]
        except KeyError:
            raise InvalidEmergeFlagNameError(self.flag_name)


class EmergeCommand():
    def __init__(self, thing: str, emerge_opts: EmergeOpts) -> None:
        self.settings: EmergeOpts = emerge_opts
        self.thing: str = thing

    def flag_sanity_check(self):
        pass

    def __repr__(self) -> str:
        return f"emerge {self.thing}"
