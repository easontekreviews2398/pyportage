# Theoretical Usage:
`pyportage --info` (for emerge --info)
`pyportage --showuse` (show USE Flags)
`pyportage --help` / `--usage` (help, usage [duh])

`pyportage merge` (basically `emerge`)
  * `--ask`
  * `--jobs`
  * `--with-bdeps`
  * `--without-bdeps`
  * `--nospinner`
  * `--deep`
  * `--newuse`
  * `--verbose`
  * `--columns`
`pyportage overlay` (basically `layman`)
  * layman
`pyportage conf-dispatch`
  * `dispatch-conf`
`pyportage webrsync`
  * basically `emerge-webrsync`
`pyportage eselect` (mappings for `eselect`)

# Handling of existing features:
either:
  * bind existing features of portage directly to pyportage
  * create new logic

# Feature Ideas:
`--editconfig`, `--edituse` and `--edit-editor` options:
  * allowing users to edit the config files with their chosen `EDITOR` envvar
  * allowing users to set the `EDITOR` envvar just in case if they are stupid
`--pkgsearch`:
  * search a package with a custom search engine, creating and polling a custom database just for this after a `--sync`
`--fix-configs`:
  * use `dispatch-conf` or a custom tool to dispatch configs
`--adduse` and `--removeuse`:
  * flag to add a use flag for a package
  * append -global to the flag to add to the global use flag in make.conf **(DANGEROUS)**

